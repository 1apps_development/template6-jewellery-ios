//
//  ForgotPasswordViewController.swift
//  Jewellery
//
//  Created by Vrushik on 20/04/22.
//

import UIKit
import Alamofire

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblInfoMsg: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickSendCode(_ sender: Any){
        if txtEmail.text! == ""{
            self.view.makeToast("Email Field should not be empty")
        }else if txtEmail.text! == "" && txtEmail.text!.isValidEmailBoth(str: txtEmail.text!){
            self.view.makeToast("Please enter valid email")
        }else{
            self.MakeAPICallforSendOTP(["email":txtEmail.text!])
        }
        
    }
  
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickContactUS(_ sender: Any){
        
    }
    
    //MARK: - API
    
    func MakeAPICallforSendOTP(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_SendOTP, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let status = json["status"] as? Int
                    if status == 1{
                        if let data = json["data"] as? [String:Any]{
                            if let msg = data["message"] as? String
                            {
                                self.view.makeToast(msg)
                                self.lblInfoMsg.text = "We've send varification to your email: \(self.txtEmail.text!), not you?"
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                    let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
                                    vc.email = self.txtEmail.text!
                                    self.navigationController?.pushViewController(vc, animated: true)
                                })
                            }
                        }
                    }else{
                        
                    }
                    
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
