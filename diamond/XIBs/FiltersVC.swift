//
//  FiltersVC.swift
//  kiddos
//
//  Created by mac on 15/04/22.
//

import UIKit
import TagListView
import RangeSeekSlider

protocol FilterDelegate {
    func filterData(tag:String,min_price:String,max_price:String,rating:String,isfilter:String)
}

class FiltersVC: UIViewController {
    
    @IBOutlet weak var lbl_minPrice: UILabel!
    @IBOutlet weak var lbl_maxPrice: UILabel!
    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    @IBOutlet weak var btn_2: UIButton!
    @IBOutlet weak var btn_1: UIButton!
    @IBOutlet weak var btn_5: UIButton!
    @IBOutlet weak var btn_4: UIButton!
    @IBOutlet weak var btn_3: UIButton!
    @IBOutlet weak var taglist: TagListView!
    
    private var selected = [String]()
    private var titles = [String]()

    var categoryArray: [categoryModel] = []
    var catdata: categoryModel?
    var maxmumPrice = String()
    var minimumPrice = String()
    var rateing = String()
    var isFilered = String()
    var selectedID = [String]()
    var selectedName = [String]()
    var delegate: FilterDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rangeSlider.delegate = self
        self.btn_1.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_2.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_3.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_4.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_5.setImage(UIImage.init(named: "checkbox"), for: .normal)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let param: [String:Any] = ["theme_id": APP_THEME_ID]
        getfilterData(param)
        let catlistParam: [String:Any] = ["theme_id": APP_THEME_ID]
        getCatgoryList(catlistParam)
    }

    @IBAction func onClickClose(_ sende: Any){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickDeleteFilters(_ sende: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickFilters(_ sende: UIButton){
        self.dismiss(animated: true) {
            if self.rateing == ""{
                self.rateing = "0"
            }
            self.isFilered = "1"
            print("\(self.selectedID)")
            self.delegate.filterData(tag: self.selectedID.joined(separator: ","), min_price: self.minimumPrice, max_price: self.maxmumPrice, rating: self.rateing, isfilter: self.isFilered)
        }
    }
    
    @IBAction func onclick_btn1(_ sender: UIButton) {
        self.rateing = "1"
        self.btn_1.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
        self.btn_2.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_3.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_4.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_5.setImage(UIImage.init(named: "checkbox"), for: .normal)
    }
    
    @IBAction func onclick_btn4(_ sender: UIButton) {
        self.rateing = "4"
        self.btn_4.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
        self.btn_3.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_2.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_5.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_1.setImage(UIImage.init(named: "checkbox"), for: .normal)
    }
    
    @IBAction func onclick_btn2(_ sender: UIButton) {
        self.rateing = "2"
        self.btn_2.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
        self.btn_1.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_3.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_4.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_5.setImage(UIImage.init(named: "checkbox"), for: .normal)
    }
    
    @IBAction func onclick_btn3(_ sender: UIButton) {
        self.rateing = "3"
        self.btn_3.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
        self.btn_2.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_4.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_5.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_1.setImage(UIImage.init(named: "checkbox"), for: .normal)
    }
    
    @IBAction func onclickBtn_5(_ sender: UIButton) {
        self.rateing = "5"
        self.btn_5.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
        self.btn_4.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_3.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_2.setImage(UIImage.init(named: "checkbox"), for: .normal)
        self.btn_1.setImage(UIImage.init(named: "checkbox"), for: .normal)
    }
    
    func getfilterData(_ param: [String:Any]){
            AIServiceManager.sharedManager.callPostApi(URL_HomeCategories, params: param, nil) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String: Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let categories = data["data"] as? [[String:Any]]{
                                        self.categoryArray = HomeCatSuperModel.init(categories).categories
                                        var arrCategories = [String]()
                                        for datafilter in categories {
                                            if let tags = datafilter["name"] as? String{
                                                arrCategories.append(tags)
                                            }
                                        }
                                        self.taglist.delegate = self
                                        self.taglist.addTags(arrCategories)
                                        self.taglist.alignment = .left
                                        self.taglist.textFont = UIFont(name: "Outfit-Medium", size: 14)!
                                        self.taglist.borderWidth = 1
                                        self.taglist.layer.borderWidth = 0.0
                                    }
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
    //                    print(json)
                    }
                case let .failure(error):
                    print(error.localizedDescription)
                    HIDE_CUSTOM_LOADER()
                    break
                }
        }
    }
    func getCatgoryList(_ param: [String:Any]) {
        AIServiceManager.sharedManager.callPostApi(URL_CATEGORYLIST, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            let maxiMumPrice = json["max_price"] as? Int
                            self.rangeSlider.maxValue = CGFloat(maxiMumPrice!)
                            self.lbl_maxPrice.text = "\(maxiMumPrice!) \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)!)"
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                   
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
    }

}
extension FiltersVC: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        //        print("Tag pressed: \(title), \(tagView.tag)")
        if tagView.isSelected == true
        {
            if selectedName.contains(title) == true
            {
                let index = selectedName.firstIndex(of: title)
                self.selectedID.remove(at: index!)
                self.selectedName.remove(at: index!)
            }
        }
        else{
            for data in self.categoryArray
            {
                if data.name == title
                {
                    self.selectedID.append("\(data.id!)")
                    self.selectedName.append(data.name!)
                }
            }
        }
        tagView.isSelected = !tagView.isSelected
    }
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        //        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}
extension FiltersVC: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === rangeSlider {
            let MinPrice = Int(minValue)
            self.minimumPrice = "\(MinPrice)"
            self.lbl_minPrice.text = "\(MinPrice) \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)!)"
            
            let MaxPrice = Int(maxValue)
            self.maxmumPrice = "\(MaxPrice)"
            self.lbl_maxPrice.text = "\(MaxPrice) \(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)!)"
            
            print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
