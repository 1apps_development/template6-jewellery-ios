//
//  ChangeAddressVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import iOSDropDown
import Alamofire

class ChangeAddressVC: UIViewController {
    
    
    @IBOutlet weak var txt_saveaddress: UITextField!
    @IBOutlet weak var txt_address: UITextField!
    @IBOutlet weak var txt_postcode: UITextField!
    @IBOutlet weak var txt_country: DropDown!
    @IBOutlet weak var txt_satate: DropDown!
    @IBOutlet weak var txt_City: DropDown!
    @IBOutlet weak var btn_yes: UIButton!
    @IBOutlet weak var btn_no: UIButton!
    
    // MARK: - Variables
    var ArrayCountry: [CountryLists] = []
    var ArrayState: [StateLists] = []
    var ArrayCity: [CityLists] = []
    var selectedCountryid = Int()
    var selectedStateid = Int()
    var selectedCityid: Any?
    var defaultaddresss: Int = 0
    var isedit = Int()
    var EditedData = AddressLists()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn_yes.setImage(UIImage(named: "radio_uncheck"), for: .normal)
        self.btn_no.setImage(UIImage(named: "radio_checked"), for: .normal)
        txt_postcode.delegate = self
        if isedit == 1 {
            self.txt_saveaddress.text = EditedData.title
            self.txt_address.text = EditedData.address
            self.txt_postcode.text = "\(EditedData.postcode!)"
            self.txt_satate.text = EditedData.statename
            self.txt_country.text = EditedData.countryname
            self.txt_City.text = EditedData.cityname
            self.selectedStateid = EditedData.stateid!
            self.selectedCountryid = EditedData.countryid!
            self.selectedCityid = EditedData.cityid
            if self.EditedData.defaultaddress == 1
            {
                self.btn_yes.setImage(UIImage.init(named: "radio_checked"), for: .normal)
                self.btn_no.setImage(UIImage.init(named: "radio_uncheck"), for: .normal)
                self.defaultaddresss = 1
            }
            else{
                self.btn_no.setImage(UIImage.init(named: "radio_checked"), for: .normal)
                self.btn_yes.setImage(UIImage.init(named: "radio_uncheck"), for: .normal)
                self.defaultaddresss = 0
            }
        }
        else{
            self.btn_yes.setImage(UIImage.init(named: "radio_uncheck"), for: .normal)
            self.btn_no.setImage(UIImage.init(named: "radio_checked"), for: .normal)
            self.defaultaddresss = 0
        }
        let param: [String:Any] = ["theme_id": APP_THEME_ID]
        getCountries(param)
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnYes(_ sender: Any){
        self.defaultaddresss = 1
        self.btn_yes.setImage(UIImage(named: "radio_checked"), for: .normal)
        self.btn_no.setImage(UIImage(named: "radio_uncheck"), for: .normal)
    }
    @IBAction func onClickBtnNo(_ sender: Any){
        self.defaultaddresss = 0
        self.btn_yes.setImage(UIImage(named: "radio_uncheck"), for: .normal)
        self.btn_no.setImage(UIImage(named: "radio_checked"), for: .normal)
    }
    @IBAction func onClickCountry(_ sender: DropDown){
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.txt_country.textColor = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_country.checkMarkEnabled = false
        self.txt_country.optionArray = countryName
        self.txt_country.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_country.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_country.didSelect(completion: { selected, index, id in
            self.txt_country.text = selected
            self.selectedCountryid = self.ArrayCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedCountryid,
                                       "theme_id": APP_THEME_ID]
            self.getState(param)
               })
    }
    @IBAction func onClickState(_ sender: DropDown){
        var statename = [String]()
        for state in ArrayState{
            statename.append(state.name!)
        }
        self.txt_satate.textColor = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_satate.checkMarkEnabled = false
        self.txt_satate.optionArray = statename
        self.txt_satate.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_satate.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_satate.didSelect(completion: { selected, index, id in
            self.txt_satate.text = selected
            self.selectedStateid = self.ArrayState[index].id!
            let param: [String:Any] = ["state_id": self.selectedStateid,
                                       "theme_id": APP_THEME_ID]
            self.getCity(param)
               })
    }
    @IBAction func onClickCity(_ sender: DropDown){
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.txt_City.textColor = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_City.checkMarkEnabled = false
        self.txt_City.optionArray = cityname
        self.txt_City.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_City.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.txt_City.didSelect(completion: { selected, index, id in
            self.txt_City.text = selected
//            self.selectedCityid = self.ArrayCity[index].id!
               })
    }
    @IBAction func onClicksaveChanges(_ sender: DropDown){
        if self.txt_address.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter save address.")
        }
        else if self.txt_saveaddress.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter address.")
        }
        else if self.txt_country.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter country.")
        }
        else if self.txt_satate.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter region / state.")
        }
        else if self.txt_City.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter city.")
        }
        else if self.txt_postcode.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter promocode.")
        }else{
            if self.isedit == 1{
                if IS_DEMO_MODE == true {
                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "You can't access this functionality as a demo user")
                }else{
                    let param: [String:Any] = ["user_id": getID(),
                                               "title": txt_saveaddress.text!,
                                               "address": txt_address.text!,
                                               "country": selectedCountryid,
                                               "state": selectedStateid,
                                               "city": selectedCityid == nil ? txt_City.text! : selectedCityid as Any,
                                               "postcode": txt_postcode.text!,
                                               "default_address": defaultaddresss,
                                               "theme_id": APP_THEME_ID]
                    getupdatedAddress(param)
                }
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "title": txt_saveaddress.text!,
                                           "address": txt_address.text!,
                                           "country": selectedCountryid,
                                           "state": selectedStateid,
                                           "city": selectedCityid == nil ? txt_City.text! : selectedCityid as Any,
                                           "postcode": txt_postcode.text!,
                                           "default_address": defaultaddresss,
                                           "theme_id": APP_THEME_ID]
                getaddAddress(param)
            }
        }
    }
    
    // MARK: - APIFunctions
    func getaddAddress(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_Addaddress, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                               let massage = data["message"] as? String
                               print(massage)
                                self.navigationController?.popViewController(animated: true)
                           }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
                }
            }
        }
    }
    func getupdatedAddress(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_UpdatedAddress, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let massage = data["message"] as? String
                                print(massage)
                                self.navigationController?.popViewController(animated: true)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
                }
            }
        }
    }
    func getCountries(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Country, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayCountry = Country.init(data).countrylists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    func getState(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_State, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayState = State.init(data).statelists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    
   func getCity(_ param: [String:Any]){
       AIServiceManager.sharedManager.callPostApi(URL_City, params: param, nil) { response in
           switch response.result{
           case let .success(result):
               HIDE_CUSTOM_LOADER()
               if let json = result as? [String: Any]{
                   if let status = json["status"] as? Int{
                       if status == 1{
                           if let data = json["data"] as? [[String:Any]]{
                               self.ArrayCity = City.init(data).citylists
                           }
                       }else if status == 9 {
                           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                           let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                           let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                           nav.navigationBar.isHidden = true
                           keyWindow?.rootViewController = nav
                       }else{
                           let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                       }
                   }
//                    print(json)
               }
           case let .failure(error):
               print(error.localizedDescription)
               HIDE_CUSTOM_LOADER()
               break
           }
       }
    }

}
extension ChangeAddressVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString = txt_postcode.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
