//
//  MainTabViewController.swift
//  Jewellery
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import SOTabBar

//172C4E

class MainTabViewController: SOTabBarController {
    
    override func loadView() {
        super.loadView()
        SOTabBarSetting.tabBarHeight = 65
        SOTabBarSetting.tabBarBackground = hexStringToUIColor(hex: "FFFFFF")
        SOTabBarSetting.tabBarTintColor = #colorLiteral(red: 0.9490196078, green: 0.8745098039, blue: 0.8078431373, alpha: 1)
        SOTabBarSetting.tabBarCircleSize = CGSize(width: 50, height: 50)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        let bestsellerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BestsellersVC")
        let invoicesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SpecialGiftsVC")
        let dashbordvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesVC")
        let usersvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WishlistVC")
//        let trackervc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrackerViewController")
        let settingsvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC")
       
    //    let image = UIImage.init(named: "tab_tickets")?.imageWithColor(color1: hexStringToUIColor(hex: "717887"))
        let image = UIImage.init(named: "tab_bestsellers")
        
        bestsellerVC.tabBarItem = UITabBarItem(title: "Bestsellers", image: image, selectedImage: UIImage(named: "tab_bestsellers"))
        invoicesVC.tabBarItem = UITabBarItem(title: "Special Gifts", image: UIImage(named: "tab_gifts"), selectedImage: UIImage(named: "tab_gifts"))
        dashbordvc.tabBarItem = UITabBarItem(title: "Categories", image: UIImage(named: "tab_categories"), selectedImage: UIImage(named: "tab_categories"))
        usersvc.tabBarItem = UITabBarItem(title: "Engagement", image: UIImage(named: "tab_engage"), selectedImage: UIImage(named: "tab_engage"))
        settingsvc.tabBarItem = UITabBarItem(title: "Wedding", image: UIImage(named: "tab_wedding"), selectedImage: UIImage(named: "tab_wedding"))
//        trackervc.tabBarItem = UITabBarItem(title: "Tracker", image: UIImage(named: "tab_tracker"), selectedImage: UIImage(named: "clock"))

           
        viewControllers = [bestsellerVC, invoicesVC,dashbordvc,usersvc ,settingsvc]
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "tab_notification"), object: nil)

    }
   @objc func showSpinningWheel(_ notification: NSNotification) {

     if let indx = notification.userInfo?["tab"] as? Int {
         self.tabBar(SOTabBar(), didSelectTabAt: indx)

     // do something with your image
     }
    }

    
}

extension MainTabViewController: SOTabBarControllerDelegate {
    func tabBarController(_ tabBarController: SOTabBarController, didSelect viewController: UIViewController) {
//        print(viewController.tabBarItem.title ?? "")
    }
}
