//
//  CategoriesDetailVC.swift
//  kiddos
//
//  Created by mac on 18/04/22.
//

import UIKit

class CategoriesDetailVC: UIViewController {
    
    @IBOutlet weak var CVProducts: UICollectionView!
    @IBOutlet weak var lbl_cartCount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CVProducts.register(UINib.init(nibName: "ProductsCell", bundle: nil), forCellWithReuseIdentifier: "ProductsCell")

        // Do any additional setup after loading the view.
    }
    @IBAction func onclickCart(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
}

extension CategoriesDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: 350)
    }

}
