//
//  AddReViewVC.swift
//  jewellery
//
//  Created by mac on 24/11/22.
//

import UIKit

protocol FeedbackDelegate {
    func refreshData(id:Int,rating_no:String,title:String,description:String)
}

class AddReViewVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var viewrateing: CosmosView!
    @IBOutlet weak var textAboutrate: UITextView!
    @IBOutlet weak var text_addRewords: UITextField!
    
    var delegate: FeedbackDelegate?
    var product_id = Int()
    var rattingscount = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textAboutrate.text = "Leave a message, if you want"
        self.textAboutrate.textColor = UIColor.lightGray
        self.textAboutrate.delegate = self
        viewrateing.didFinishTouchingCosmos = { rating in
            print(rating)
            self.rattingscount = "\(rating)"
        }
        // Do any additional setup after loading the view.
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.init(named: "App_bg_Color")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Leave a message, if you want"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func onclickCancel(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickRatenow(_ sender: UIButton){
        if self.textAboutrate.text == "Leave a message, if you want" || self.text_addRewords.text == "" || self.rattingscount == ""
        {
            let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Please enter message", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "okay", style: .default)
            alertVC.addAction(yesAction)
            self.present(alertVC,animated: true,completion: nil)
        }
        else{
            if self.textAboutrate.text == "Leave a message, if you want"
            {
                self.textAboutrate.text = ""
            }
            self.delegate?.refreshData(id: self.product_id, rating_no: self.rattingscount, title: self.text_addRewords.text!, description: self.textAboutrate.text!)

            self.navigationController?.popViewController(animated: false)
            
        }
    }
}
