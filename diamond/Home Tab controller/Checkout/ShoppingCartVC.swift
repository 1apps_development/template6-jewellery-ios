//
//  ShoppingCartVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit
import Alamofire

class ShoppingCartVC: UIViewController {
    
    @IBOutlet weak var tableShopping: UITableView!
    @IBOutlet weak var lbl_subtotal: UILabel!
    @IBOutlet weak var lbl_totalprice: UILabel!
    @IBOutlet weak var txt_cupenCode: UITextField!
    @IBOutlet weak var ShoppingTVheight: NSLayoutConstraint!
    @IBOutlet weak var lbl_totalproduct: UILabel!
    @IBOutlet weak var CVTextInfo: UICollectionView!
    @IBOutlet weak var lbl_CupenDiscount: UILabel!
    @IBOutlet weak var lbl_cupencode: UILabel!
    @IBOutlet weak var lbl_nodataFound: UILabel!
    @IBOutlet weak var view_cupencount: UIView!
    @IBOutlet weak var view_cupendiscount: UIView!
    @IBOutlet weak var view_empty: UIView!
    
    //MARK: - Variables
    var GuestCartList_Array: [CartsList] = []
    var cartListArray: [CartsList] = []
    var textinfoArray: [TextdataList] = []
    var sub_total = String()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_CupenDiscount.isHidden = true
        lbl_cupencode.isHidden = true
        view_cupencount.isHidden = true
        view_cupendiscount.isHidden = true
        lbl_nodataFound.isHidden = true
        view_empty.isHidden = true
        CVTextInfo.register(UINib(nibName: "TextDetailsCell", bundle: nil), forCellWithReuseIdentifier: "TextDetailsCell")
        tableShopping.register(UINib.init(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
        tableShopping.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view_cupendiscount.isHidden = false
        self.view_cupencount.isHidden = false
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                let Guest_Array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                self.GuestCartList_Array = ProductsList.init(Guest_Array).cartsList
                getUpdatedValue()
            }
        }
        else{
            let param: [String:Any] = ["user_id": getID(),
                                       "theme_id": APP_THEME_ID]
            getCartList(param)
        }
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickApplyCupenCode(_ sender: Any){
        if txt_cupenCode.text == ""{
            showAlertMessage(titleStr: "", messageStr: "Please enter promocode.")
        }else{
            let param: [String:Any] = ["coupon_code": self.txt_cupenCode.text!,
                                       "sub_total": self.sub_total,
                                       "theme_id": APP_THEME_ID]
            getApplyCupenCode(param)
        }
    }
    @IBAction func onClicProcedCheakOut(_ sender: Any){
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) == nil{
                let guest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                self.GuestCartList_Array = ProductsList.init(guest_array).cartsList
                
                var productarray = [[String:Any]]()
                var arrtextinfo = [[String:Any]]()
                
                for i in GuestCartList_Array{
                    let prodId = i.productid!
                    let qty = i.qty!
                    let varientid = i.varientId!
                    let productobject: [String:Any] = ["product_id": prodId, "qty": qty, "variant_id": varientid]
                    print(productobject)
                    productarray.append(productobject)
                }
                UserDefaults.standard.set(productarray, forKey: userDefaultsKeys.KEY_GESTUSERPRODUCTARRAY)
                for textinfodata in self.textinfoArray {
                    let textInfoObject = ["tax_string": "\(textinfodata.taxString!)", "tax_price": "\(textinfodata.taxPrice!)", "tax_type": "\(textinfodata.textype!)", "tax_name": "\(textinfodata.taxname!)", "tax_amount": "\(textinfodata.taxAmmount!)", "id": "\(textinfodata.taxid!)"]
                    print(textInfoObject)
                    arrtextinfo.append(textInfoObject)
                }
                UserDefaults.standard.set(arrtextinfo, forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)
                let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
                let photoLibraryAction = UIAlertAction(title: "Countinue as guest", style: .default) { (action) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "GuestAddressVC") as! GuestAddressVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                let cameraAction = UIAlertAction(title: "Countinue to sign in", style: .default) { (action) in
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                    nav.navigationBar.isHidden = true
                    keyWindow?.rootViewController = nav

                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                alert.addAction(photoLibraryAction)
                alert.addAction(cameraAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let param: [String:Any] = ["user_id": getID(),
                                       "theme_id": APP_THEME_ID]
            getcartCheck(param)
        }
    }
    
    //MARK: - APIfunction
    func getguestUserData(_ param: [String:Any]){
                AIServiceManager.sharedManager.callPostApi(URL_taxGuest, params: param, nil) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let finalPrice = data["final_price"] as? String
                                        let convertInt = Int(Float(finalPrice!)!)
                                        let orignalPrice = data["original_price"] as? String
                                        self.sub_total = orignalPrice!
                                        self.lbl_subtotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(orignalPrice!)"
                                        self.lbl_totalprice.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(finalPrice!)"
                                        UserDefaults.standard.set(self.lbl_subtotal.text, forKey: userDefaultsKeys.KEY_GUESTSUBTOTAL)
                                        UserDefaults.standard.set(self.lbl_totalprice.text, forKey: userDefaultsKeys.KEY_TOTALPRICE)
                                        UserDefaults.standard.set(convertInt, forKey: userDefaultsKeys.KEY_GUESTFINALTOTAL)
                                        self.tableShopping.reloadData()
                                        self.ShoppingTVheight.constant = CGFloat(self.GuestCartList_Array.count * 90)
                                        if let textdatas = data["tax_info"] as? [[String:Any]]{
                                            self.textinfoArray = TextData.init(textdatas).textdatalists
                                            self.CVTextInfo.reloadData()
                                        }
                                    }
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
        //                    print(json)
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
            }
          }
    func getCartList(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartList, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    if let products = stats["product_list"] as? [[String:Any]]{
                                        self.cartListArray = ProductsList.init(products).cartsList
                                        self.tableShopping.reloadData()
                                    }
                                    let txtCart = stats["cart_total_qty"] as? Int
                                    self.lbl_totalproduct.text = "\(txtCart!)"
                                    let orignalPrice = stats["original_price"] as? String
                                    self.sub_total = orignalPrice!
                                    if let textdatas = stats["tax_info"] as? [[String:Any]]{
                                        self.textinfoArray = TextData.init(textdatas).textdatalists
                                        self.CVTextInfo.reloadData()
                                    }
                                    let productprice = stats["final_price"] as! String
                                    let convertInt = Int(Float(productprice)!)
                                    UserDefaults.standard.set(convertInt, forKey: userDefaultsKeys.KEY_GUESTFINALTOTAL)
                                    let subTotalPrice = stats["sub_total"] as! Double
                                    self.lbl_totalprice.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(productprice)"
                                    self.lbl_subtotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(subTotalPrice)"
        //                            self.totalPrice.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY) ?? "$")\(subtotal!)"
                                    let totalproducts = stats["cart_total_qty"] as? Int
                                    self.lbl_totalproduct.text = "\(totalproducts!)"
                                    self.tableShopping.reloadData()
        //                            self.lblNodataFound.isHidden = false
                                    self.ShoppingTVheight.constant = CGFloat(self.cartListArray.count * 90)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                let param: [String:Any] = ["user_id": getID(),
                                                           "theme_id": APP_THEME_ID]
                                self.getCartList(param)
                                self.tableShopping.reloadData()
        //                        print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getApplyCupenCode(_ param: [String:Any]){
            AIServiceManager.sharedManager.callPostApi(URL_CupenCode, params: param, nil) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let id = stats["id"] as? Int
                                    let name = stats["name"] as? String
                                    let code = stats["code"] as? String
                                    let discountType = stats["coupon_discount_type"] as? String
                                    let discountAmount = stats["coupon_discount_amount"] as? Double
                                    if discountType == "percentage" {
                                        self.lbl_CupenDiscount.text = "-\(discountAmount!)%"
                                    }else{
                                        self.lbl_CupenDiscount.text = "-\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(discountAmount!)"
                                    }
                                    let discountFinalAmount = stats["final_price"] as? Double
                                    let discoutAble = stats["amount"] as? String
                                    
                                    let couponObj = ["coupon_id": id!, "coupon_name": name!, "coupon_code": code!, "coupon_discount_type": discountType!, "coupon_discount_amount": discountAmount!, "coupon_final_amount": discountFinalAmount!, "coupon_discount_number": discoutAble!] as [String : Any]
                                    
                                    self.lbl_totalprice.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(discountFinalAmount!)"
//                                    self.lbl_subtotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(subtotal!)"
//                                    self.lbl_totalprice.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(finalprice!)"
        //                            self.lblvatvalue.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(discountprice!)"
                                    UserDefaults.standard.set(discountFinalAmount, forKey: userDefaultsKeys.KEY_GUESTFINALTOTAL)
                                    UserDefaults.standard.set(self.lbl_totalprice.text, forKey: userDefaultsKeys.KEY_GUESTSUBTOTAL)
                                    UserDefaults.standard.set(couponObj, forKey: userDefaultsKeys.KEY_CUPENOBJECT)
                                    
                                    self.lbl_cupencode.isHidden = false
                                    self.lbl_CupenDiscount.isHidden = false
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
    }
    
    func getcartCheck(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_CartCheak, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as! String
                                    print(massage)
                                }
                                let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "BillingAddressVC") as! BillingAddressVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    //MARK: - Function
    func getUpdatedValue(){
        var totalvalue: Double = 0
        var qtyTotal: Int = 0
        for productQty in GuestCartList_Array{
            totalvalue += Double(productQty.qty!) * Double(productQty.finalprice!)!
            qtyTotal += Int(productQty.qty!)
        }
        self.lbl_totalprice.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(totalvalue)"
        self.lbl_totalproduct.text = "\(qtyTotal)"
        let param: [String:Any] = ["sub_total": totalvalue,
                                   "theme_id": APP_THEME_ID]
        getguestUserData(param)
    }
}
extension UISwipeActionsConfiguration {
    
    public static func makeTitledImage(
        image: UIImage?,
        title: String,
        textColor: UIColor?,
        font: UIFont = UIFont.systemFont(ofSize: 0.0),
        size: CGSize = .init(width: 50, height: 50)
    ) -> UIImage? {
        
        /// Create attributed string attachment with image
        let attachment = NSTextAttachment()
        attachment.image = image
        let imageString = NSAttributedString(attachment: attachment)
        
        /// Create attributed string with title
        let text = NSAttributedString(
            string: "\n\(title)",
            attributes: [
                .foregroundColor: textColor!,
                .font: font
            ]
        )
        
        /// Merge two attributed strings
        let mergedText = NSMutableAttributedString()
        mergedText.append(imageString)
        mergedText.append(text)
        
        /// Create label and append that merged attributed string
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        label.textAlignment = .center
        label.numberOfLines = 2
        label.attributedText = mergedText
        
        /// Create image from that label
        let renderer = UIGraphicsImageRenderer(bounds: label.bounds)
        let image = renderer.image { rendererContext in
            label.layer.render(in: rendererContext.cgContext)
        }
        
        /// Convert it to UIImage and return
        if let cgImage = image.cgImage {
            return UIImage(cgImage: cgImage, scale: UIScreen.main.scale, orientation: .up)
        }
        
        return nil
    }
    
}
extension ShoppingCartVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            if self.GuestCartList_Array.count == 0 {
                self.view_empty.isHidden = false
                self.lbl_nodataFound.isHidden = false
            }else{
                self.view_empty.isHidden = true
                self.lbl_nodataFound.isHidden = true
                return self.GuestCartList_Array.count
            }
        }
        else
        {
            if self.cartListArray.count == 0 {
                self.view_empty.isHidden = false
                self.lbl_nodataFound.isHidden = false
            }else{
                self.view_empty.isHidden = true
                self.lbl_nodataFound.isHidden = true
                return self.cartListArray.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            cell.configCell(GuestCartList_Array[indexPath.row])
            
            if GuestCartList_Array[indexPath.row].qty! <= 1 {
                cell.btn_proMinus.isEnabled = false
            }else{
                cell.btn_proMinus.isEnabled = true
            }
        }else{
            let data = cartListArray[indexPath.row]
            cell.lbl_productName.text = data.name
            let productimgURL = getImageFullURL("\(data.image!)")
            cell.img_products.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
                cell.img_products.image = image
            }
//            let price = Double(data.finalprice!)! * Double(data.qty!)
            cell.lbl_proprice.text = cartListArray[indexPath.row].varientName
            cell.lbl_finalprice.text = "\(data.finalprice!)"
            cell.lbl_provalue.text = "\(data.qty!)"
            cell.lbl_proCurrency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)
            
            if data.qty! <= 1 {
                cell.btn_proMinus.isEnabled = false
            }else{
                cell.btn_proMinus.isEnabled = true
            }
        }
        cell.btn_proMinus.tag = indexPath.row
        cell.btn_proMinus.addTarget(self, action: #selector(onclickMinus), for: .touchUpInside)
        cell.btn_Plus.tag = indexPath.row
        cell.btn_Plus.addTarget(self, action: #selector(onclickPlus), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteaction = UIContextualAction(style: .normal, title: nil) { (_, _, success: (Bool) -> Void) in
            success(true)
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) == nil{
                    var GuestCartData = [[String:Any]]()
                    GuestCartData = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                    GuestCartData.remove(at: indexPath.row)
                    UserDefaults.standard.set(GuestCartData, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                    self.GuestCartList_Array = ProductsList.init(GuestCartData).cartsList
                    UserDefaults.standard.set(self.GuestCartList_Array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                    self.getUpdatedValue()
                }
            }else{
                let data = self.cartListArray[indexPath.row]
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.productid!,
                                           "variant_id": data.varientId!,
                                           "quantity_type": "remove",
                                           "theme_id": APP_THEME_ID]
                self.getQtyOfProduct(param)
            }
        }
        deleteaction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_delete"),
            title: "", textColor: UIColor(red: 242/255, green: 233/255, blue: 206/255))
        deleteaction.backgroundColor = hexStringToUIColor(hex: "#FBD9B3")
    
        return UISwipeActionsConfiguration(actions: [deleteaction])
    }
    @objc func onclickMinus(sender:UIButton){
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                var GuestCartData = [[String:Any]]()
                GuestCartData = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String : Any]]
                var cartobject = GuestCartData[sender.tag]
                var qty = cartobject["qty"] as? Int
                qty! -= 1
                cartobject["qty"] = qty
                GuestCartData.remove(at: sender.tag)
                GuestCartData.insert(cartobject, at: sender.tag)

                self.GuestCartList_Array = ProductsList.init(GuestCartData).cartsList
                self.tableShopping.reloadData()
                UserDefaults.standard.set(GuestCartData, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                getUpdatedValue()
            }
        }else{
            let param: [String:Any] = ["user_id": getID(),
                                       "product_id": cartListArray[sender.tag].productid!,
                                       "variant_id": cartListArray[sender.tag].varientId!,
                                       "quantity_type": "decrease",
                                       "theme_id": APP_THEME_ID]
            getQtyOfProduct(param)
        }
    }
    @objc func onclickPlus(sender:UIButton){
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                var GuestCartData = [[String:Any]]()
                GuestCartData = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String : Any]]
                var cartobject = GuestCartData[sender.tag]
                var qty = cartobject["qty"] as? Int
                qty! += 1
                cartobject["qty"] = qty
                GuestCartData.remove(at: sender.tag)
                GuestCartData.insert(cartobject, at: sender.tag)

                self.GuestCartList_Array = ProductsList.init(GuestCartData).cartsList
                UserDefaults.standard.set(GuestCartData, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                //               setValueToUserDefaults(value: GuestCartList_Array as AnyObject, key: userDefaultsKeys.KEY_SAVED_CART)
                getUpdatedValue()
            }
        }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": cartListArray[sender.tag].productid!,
                                           "variant_id": cartListArray[sender.tag].varientId!,
                                           "quantity_type": "increase",
                                           "theme_id": APP_THEME_ID]
                getQtyOfProduct(param)
            }
        }
}
extension ShoppingCartVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textinfoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextDetailsCell", for: indexPath) as! TextDetailsCell
        cell.configureCell(textinfoArray[indexPath.row])
        return cell
    }
}
