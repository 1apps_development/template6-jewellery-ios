//
//  GuestAddressVC.swift
//  jewellery
//
//  Created by mac on 22/11/22.
//

import UIKit
import iOSDropDown

class GuestAddressVC: UIViewController {
    
    @IBOutlet weak var text_fname: UITextField!
    @IBOutlet weak var text_lname: UITextField!
    @IBOutlet weak var text_telephone: UITextField!
    @IBOutlet weak var text_email: UITextField!
    
    @IBOutlet weak var text_BillCompany: UITextField!
    @IBOutlet weak var text_BillAddress: UITextField!
    @IBOutlet weak var text_BillPostcode: UITextField!
    @IBOutlet weak var text_BillCountry: DropDown!
    @IBOutlet weak var text_BillState: DropDown!
    @IBOutlet weak var text_BillCity: DropDown!
    
    @IBOutlet weak var text_dillCompany: UITextField!
    @IBOutlet weak var text_dillAddress: UITextField!
    @IBOutlet weak var text_dillPostCode: UITextField!
    @IBOutlet weak var text_dillCountry: DropDown!
    @IBOutlet weak var text_dillState: DropDown!
    @IBOutlet weak var text_dillCity: DropDown!
    @IBOutlet weak var delivary_view: UIView!
    @IBOutlet weak var delivary_ViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btn_selected: UIButton!
    
    
    //MARK: - Variables
    
    var selectedCountryid = Int()
    var selectedStateid = Int()
    var selectedCityid = String()
    var ArrayCountry: [CountryLists] = []
    var ArraySatate: [StateLists] = []
    var ArrayCity: [CityLists] = []
    
    var selectedDelivaryCountryid = Int()
    var selectedDelivaryStateid = Int()
    var selectedDelivaryCityid = String()
    var ArrayDelivaryCountry: [CountryLists] = []
    var ArrayDelivarySatate: [StateLists] = []
    var ArrayDelivaryCity: [CityLists] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delivary_view.isHidden = true
        self.delivary_ViewHeight.constant = 0.0
        self.text_BillPostcode.delegate = self
        self.text_dillPostCode.delegate = self
        let param: [String:Any] = ["theme_id": APP_THEME_ID]
        getCountries(param)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.btn_selected.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
    }
    @IBAction func onclick_Back(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickCheakBox(_ sender: UIButton){
        if self.btn_selected.imageView?.image == UIImage.init(named: "checkbox_selected")
        {
            self.btn_selected.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.delivary_view.isHidden = false
            self.delivary_ViewHeight.constant = 500.0
        }
        else{
            self.btn_selected.setImage(UIImage.init(named: "checkbox_selected"), for: .normal)
            self.delivary_view.isHidden = true
            self.delivary_ViewHeight.constant = 0.0
        }
    }
    @IBAction func onclickBillcountry(_ sender: DropDown){
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.text_BillCountry.textColor = hexStringToUIColor(hex: "#AFAFAF")
        self.text_BillCountry.checkMarkEnabled = false
        self.text_BillCountry.optionArray = countryName
        self.text_BillCountry.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.text_BillCountry.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.text_BillCountry.didSelect(completion: { selected, index, id in
            self.text_BillCountry.text = selected
            self.selectedCountryid = self.ArrayCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedCountryid,
                                       "theme_id": APP_THEME_ID]
            self.getState(param)
               })
    }
    @IBAction func onclickBillState(_ sender: DropDown){
        var statename = [String]()
        for state in ArraySatate{
            statename.append(state.name!)
        }
        self.text_BillState.textColor = hexStringToUIColor(hex: "#AFAFAF")
        self.text_BillState.checkMarkEnabled = false
        self.text_BillState.optionArray = statename
        self.text_BillState.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.text_BillState.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.text_BillState.didSelect(completion: { selected, index, id in
            self.text_BillState.text = selected
            self.selectedStateid = self.ArraySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedStateid,
                                       "theme_id": APP_THEME_ID]
            self.getCity(param)
               })
    }
    @IBAction func onclickBillcity(_ sender: DropDown){
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.text_BillCity.textColor = hexStringToUIColor(hex: "#AFAFAF")
        self.text_BillCity.checkMarkEnabled = false
        self.text_BillCity.optionArray = cityname
        self.text_BillCity.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.text_BillCity.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.text_BillCity.didSelect(completion: { selected, index, id in
            self.text_BillCity.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    @IBAction func onclickdillcountry(_ sender: DropDown){
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.text_dillCountry.textColor = hexStringToUIColor(hex: "#AFAFAF")
        self.text_dillCountry.checkMarkEnabled = false
        self.text_dillCountry.optionArray = countryName
        self.text_dillCountry.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.text_dillCountry.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.text_dillCountry.didSelect(completion: { selected, index, id in
            self.text_dillCountry.text = selected
            self.selectedDelivaryCountryid = self.ArrayDelivaryCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedDelivaryCountryid,
                                       "theme_id": APP_THEME_ID]
            self.getState(param)
               })
    }
    @IBAction func onclickdillState(_ sender: DropDown){
        var statename = [String]()
        for state in ArraySatate{
            statename.append(state.name!)
        }
        self.text_dillState.textColor = hexStringToUIColor(hex: "#AFAFAF")
        self.text_dillState.checkMarkEnabled = false
        self.text_dillState.optionArray = statename
        self.text_dillState.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.text_dillState.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.text_dillState.didSelect(completion: { selected, index, id in
            self.text_dillState.text = selected
            self.selectedDelivaryStateid = self.ArrayDelivarySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedDelivaryStateid,
                                       "theme_id": APP_THEME_ID]
            self.getCity(param)
               })
    }
    @IBAction func onclickdillcity(_ sender: DropDown){
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.text_dillCity.textColor = hexStringToUIColor(hex: "#AFAFAF")
        self.text_dillCity.checkMarkEnabled = false
        self.text_dillCity.optionArray = cityname
        self.text_dillCity.selectedRowColor = hexStringToUIColor(hex: "#F2DFCE")
        self.text_dillCity.rowBackgroundColor  = hexStringToUIColor(hex: "#F2DFCE")
        self.text_dillCity.didSelect(completion: { selected, index, id in
            self.text_dillCity.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    @IBAction func onclickContinue(_ sender: UIButton){
        if self.text_fname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter first name")
        }
        else if self.text_lname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter last name.")
        }
        if self.text_email.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the email address.")
        }
        else if self.text_telephone.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter phone number.")
        }
        else if isValidEmail(self.text_email.text!) == false
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
        }
        else if isValidEmail(self.text_email.text!) == false
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
        }
        else if self.text_BillAddress.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter address.")
        }
       
        else if self.text_BillCountry.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter country.")
        }
        else if self.text_BillState.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter region / state.")
        }
        else if self.text_BillCity.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter city.")
        }
        else if self.text_BillPostcode.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter postcode.")
        }
        else{
            if self.btn_selected.imageView?.image == UIImage(named: "checkbox_selected"){
                UserDefaults.standard.set(self.text_BillCountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRY)
                UserDefaults.standard.set(self.text_BillState.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATE)
                
                UserDefaults.standard.set(self.text_dillCountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRYDELIVARY)
                UserDefaults.standard.set(self.text_dillState.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATEDELIVARY)
                
                let billingObj = ["firstname":self.text_fname.text!,"lastname":self.text_lname.text!,"email":self.text_email.text!,"billing_user_telephone":self.text_telephone.text!,"billing_address":self.text_BillAddress.text!,"billing_postecode":self.text_BillPostcode.text!,"billing_country":self.selectedCountryid,"billing_state":self.selectedStateid,"billing_city":self.text_BillCity.text!,"delivery_address":self.text_BillAddress.text!,"delivery_postcode":self.text_BillPostcode.text!,"delivery_country":self.selectedCountryid,"delivery_state":self.selectedStateid,"delivery_city":self.text_BillCity.text!] as [String : Any]
                UserDefaults.standard.set(billingObj, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                if self.text_dillCompany.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery address.")
                }
                else if self.text_dillCountry.text == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery country.")
                }
                else if self.text_dillState.text == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery region / state.")
                }
                else if self.text_dillCity.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery city.")
                }
                else if self.text_dillPostCode.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery postcode.")
                }
                else{
                    UserDefaults.standard.set(self.text_BillCountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRY)
                    UserDefaults.standard.set(self.text_BillState.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATE)
                    
                    UserDefaults.standard.set(self.text_dillCountry.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRYDELIVARY)
                    UserDefaults.standard.set(self.text_dillState.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATEDELIVARY)
                    
                    let delivaryObject = ["firstname":self.text_fname.text!,"lastname":self.text_lname.text!,"email":self.text_email.text!,"billing_user_telephone":self.text_telephone.text!,"billing_address":self.text_BillAddress.text!,"billing_postecode":self.text_BillPostcode.text!,"billing_country":self.selectedCountryid,"billing_state":self.selectedStateid,"billing_city":self.text_BillCity.text!,"delivery_address":self.text_dillAddress.text!,"delivery_postcode":self.text_dillPostCode.text!,"delivery_country":self.selectedDelivaryCountryid,"delivery_state":self.selectedDelivaryStateid,"delivery_city":self.text_dillCity.text!] as [String : Any]
                    
                    UserDefaults.standard.set(delivaryObject, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    //MARK: - ApiFuctions
    func getCountries(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Country, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayCountry = Country.init(data).countrylists
                                self.ArrayDelivaryCountry = Country.init(data).countrylists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    func getState(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_State, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArraySatate = State.init(data).statelists
                                self.ArrayDelivarySatate = State.init(data).statelists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    
   func getCity(_ param: [String:Any]){
       AIServiceManager.sharedManager.callPostApi(URL_City, params: param, nil) { response in
           switch response.result{
           case let .success(result):
               HIDE_CUSTOM_LOADER()
               if let json = result as? [String: Any]{
                   if let status = json["status"] as? Int{
                       if status == 1{
                           if let data = json["data"] as? [[String:Any]]{
                               self.ArrayCity = City.init(data).citylists
                               self.ArrayDelivaryCity = City.init(data).citylists
                           }
                       }else if status == 9 {
                           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                           let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                           let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                           nav.navigationBar.isHidden = true
                           keyWindow?.rootViewController = nav
                       }else{
                           let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                       }
                   }
//                    print(json)
               }
           case let .failure(error):
               print(error.localizedDescription)
               HIDE_CUSTOM_LOADER()
               break
           }
       }
    }
    
    //MARK: - Functions
}
extension GuestAddressVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        if textField == text_dillPostCode {
            let currentString: NSString = text_dillPostCode.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        let currentString: NSString = text_BillPostcode.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
