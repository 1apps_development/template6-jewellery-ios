//
//  JewelleryDescriptionVC.swift
//  jewellery
//
//  Created by mac on 25/04/22.
//

import UIKit
import ImageSlideshow
import Alamofire
import iOSDropDown

class DropDownCell: UITableViewCell{
    
    var onclickButtonClosure: (()->Void)?
    
    @IBOutlet weak var lbl_selectColorTitle: UILabel!
    @IBOutlet weak var lbl_selectColor: UILabel!
    @IBOutlet weak var txt_selectColor: DropDown!
    @IBOutlet weak var colorSelectionView: UIView!
    
    @IBAction func onClickView(_ sender: DropDown){
        self.onclickButtonClosure?()
    }
    
}
class SizeCell: UITableViewCell{
    @IBOutlet weak var lbl_selectSize: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
}

class JewelleryDescriptionVC: UIViewController, ImageSlideshowDelegate{
    
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
       
    }

    @IBOutlet weak var lbl_categories: UILabel!
    @IBOutlet weak var lbl_SKU: UILabel!
    @IBOutlet weak var lbl_productCategories: UILabel!
    @IBOutlet weak var img_product: UIImageView!
    @IBOutlet weak var lbl_OutofStock: UILabel!
    @IBOutlet weak var cartIndexLabel: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var addReview: UIButton!
    @IBOutlet weak var varientTableView: UITableView!
    @IBOutlet weak var descriptionTableView: UITableView!
    @IBOutlet weak var height_discrition: NSLayoutConstraint!
    @IBOutlet weak var reviewCollection: UICollectionView!
    @IBOutlet weak var height_tableView: NSLayoutConstraint!
    @IBOutlet weak var height_rateing: NSLayoutConstraint!
    @IBOutlet weak var lbl_Discription: UILabel!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var lbl_productname: UILabel!
    @IBOutlet weak var bgimage: ImageSlideshow!
    @IBOutlet weak var CVMoreImages: UICollectionView!
    @IBOutlet weak var CVRelatedProducts: UICollectionView!
    @IBOutlet weak var heightRelatedPro: NSLayoutConstraint!
    @IBOutlet weak var lbl_related: UILabel!
    @IBOutlet weak var btn_addtocart: UIButton!
    
    var productId: Int = 0
    var arrProductsDetails: [Products] = []
    var arrrelatedProducts: [Products] = []
    var arrSubProduct: [ProductImages] = []
    var arrProductImages: [SDWebImageSource] = []
    var arrVarients: [Varient] = []
    var disciptions: [OtherDis] = []
    var arrReviews: [Review] = []
    var valueArray: [String] = []
    var SelectVarientArray: [String] = []
    var varientColor: [String] = []
    var cellHeight = Double()
    var varienrid: Int?
    var productDetalsId: Int?
    var varientDetailsId: Int?
    var isstock: String?
    var itemid: String?
    var catid: String?
    var Selected_Variant_Name = String()
    var stock: Int?
    var gestUserdata: [String:Any] = [:]
    var pageIndex = 1
    var lastIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_OutofStock.isHidden = true
        self.addReview.isHidden = true
        self.heightRelatedPro.constant = 0.0
        self.lbl_related.isHidden = true
        self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        CVMoreImages.register(UINib.init(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCell")
        CVRelatedProducts.register(UINib.init(nibName: "ProductsCell", bundle: nil), forCellWithReuseIdentifier: "ProductsCell")
        descriptionTableView.register(UINib(nibName: "DescriptionCell", bundle: nil), forCellReuseIdentifier: "DescriptionCell")
        reviewCollection.register(UINib(nibName: "ReViewCell", bundle: nil), forCellWithReuseIdentifier: "ReViewCell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            self.MakeAPICallforProductDetailGuest(["id":self.productId,"theme_id":APP_THEME_ID])
        }else{
           self.MakeAPICallforProductDetail(["id":self.productId,"theme_id":APP_THEME_ID])
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCartClick(_ sender : Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickCartReview(_ sender : Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "AddReViewVC") as! AddReViewVC
        vc.product_id = productId
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickAddtoCartClick(_ sender : Any){
        if self.stock == 0{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) != nil{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }else{
                let param: [String: Any] = ["product_id": productId,
                                            "user_id": getID(),
                                            "theme_id": APP_THEME_ID]
                self.notifyUser(param)
            }
        }else{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                    
                    var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                    var iscart = false
                    var cartindex = Int()
                    for i in 0..<Gest_array.count{
                        if Gest_array[i]["product_id"]! as! Int == gestUserdata["id"] as! Int && Gest_array[i]["variant_id"]! as! Int == self.varienrid!{
                            iscart = true
                            cartindex = i
                        }
                    }
                    if iscart == false{
                        let cartobj = ["product_id": gestUserdata["id"] as! Int,
                                       "image": gestUserdata["cover_image_url"] as! String,
                                       "name": gestUserdata["name"] as! String,
                                       "orignal_price": gestUserdata["original_price"] as! String,
                                       "discount_price": gestUserdata["discount_price"] as! String,
                                       "final_price": gestUserdata["final_price"] as! String,
                                       "qty": 1,
                                       "variant_id": varienrid!,
                                       "variant_name": Selected_Variant_Name] as [String : Any]
                        Gest_array.append(cartobj)
                        UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                        UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                        UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                        
                        let alert = UIAlertController(title: nil, message: "\(gestUserdata["name"] as! String) add successfully", preferredStyle: .actionSheet)
                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                            self.dismiss(animated: true)
                        }
                        
                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                        alert.addAction(photoLibraryAction)
                        alert.addAction(cameraAction)
                        alert.addAction(cancelAction)
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                        let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                            var cartsList = Gest_array[cartindex]
                            cartsList["qty"] = cartsList["qty"] as! Int + 1
                            Gest_array.remove(at: cartindex)
                            Gest_array.insert(cartsList, at: cartindex)
                            
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                        }
                        let noaction = UIAlertAction(title: "No", style: .destructive)
                        alert.addAction(yesaction)
                        alert.addAction(noaction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": productDetalsId!,
                                           "variant_id": varientDetailsId!,
                                           "qty": 1,
                                           "theme_id": APP_THEME_ID]
                MakeApiCallForAddtoCartDetails(param)
            }
        }
    }
    
    //MARK: - CustomFunction
    
    func imageSliderData() {
        self.bgimage.slideshowInterval = 3.0
        self.bgimage.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.bgimage.contentScaleMode = UIView.ContentMode.scaleAspectFit
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.bgimage.pageIndicator = pageControl
        self.bgimage.setImageInputs(self.arrProductImages)
        self.bgimage.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.bgimage.addGestureRecognizer(recognizer)
    }
    @objc func didTapImage() {
        self.bgimage.presentFullScreenController(from: self)
    }
    
    //MARK: - API
    func MakeAPICallforProductDetail(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_ProductDetail, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["product_info"] as? [String:Any]{
                                self.gestUserdata = aData
                                let proTitle = aData["name"] as? String
                                self.lbl_productname.text = proTitle
                                let proId = aData["id"] as? Int
                                self.productDetalsId = proId
                                let proDescription = aData["description"] as? String
                                self.lbl_Discription.text = proDescription
                                let proPrice = aData["final_price"] as? String
                                self.lbl_price.text = proPrice
                                let categories = aData["category_name"] as? String
                                self.lbl_productCategories.text = "Category: \(categories!)"
                                self.lbl_categories.text = categories
                                let image = aData["cover_image_path"] as? String
                                let imageURL = getImageFullURL("\(image!)")
                                self.img_product.sd_setImage(with: URL(string: imageURL)) { image, error, type, url in
                                  self.img_product.image = image
                               }
                            if let discription = aData["other_description_array"] as? [[String:Any]]{
                                self.disciptions = Discription.init(discription).discription
                                self.descriptionTableView.reloadData()
                                self.height_discrition.constant = CGFloat(self.disciptions.count * 171)
                            }
                            let stock = aData["product_stock"] as? Int
                            self.stock = stock
                            let defaultvarientid = aData["default_variant_id"] as? Int
                            self.varienrid = defaultvarientid
                            if defaultvarientid == 0 {
                                if stock! <= 0 {
                                    self.lbl_OutofStock.isHidden = false
                                    self.btn_addtocart.setTitle("Notify me when available", for: .normal)
                                }else{
                                    self.lbl_OutofStock.isHidden = true
                                    self.btn_addtocart.setTitle("Add to cart", for: .normal)
                                }
                            }else{
                                if self.arrVarients.count != 0 {
                                    self.SelectVarientArray.removeAll()
                                    self.height_tableView.constant = CGFloat(self.arrVarients.count * 100)
                                    for varient in self.arrVarients {
                                        if varient.value?.count != 0 {
                                            self.SelectVarientArray.append(varient.value![0])
                                        }
                                    }
                                    let param: [String:Any] = ["product_id": self.productId,
                                                               "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                               "theme_id": APP_THEME_ID
                                    ]
                                    self.MakeAPICallforVarientStock(param)
                                }
                            }
                            let review = aData["is_review"] as? Int
                            if review == 1 {
                                self.addReview.isHidden = true
                            }else if review == 0{
                                self.addReview.isHidden = false
                            }else{
                                self.addReview.isHidden = false
                            }
                        }
                        if let productReview = data["product_Review"] as? [[String:Any]]{
                            self.arrReviews = ProductReview.init(productReview).productreview
                            self.reviewCollection.reloadData()
                        }
                        if self.arrReviews.count != 0 {
//                            self.reteView.isHidden = false
                            self.height_rateing.constant = 270.0
                        }else{
//                            self.reteView.isHidden = true
                            self.height_rateing.constant = 0.0
                        }
                        self.arrProductImages.removeAll()
                        if let varient = data["product_image"] as? [[String:Any]]{
                            self.arrSubProduct = ProductSubImage.init(varient).subproductdata
                            let arrImage = self.arrSubProduct.map({$0.imagePath})
                            for producturl in 0..<arrImage.count{
                                let urlendpoint = arrImage[producturl]
                                if let endpoint = getImageFullURL(urlendpoint!) as? String {
                                    if let encodedURL = endpoint.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                                       let url = URL(string: encodedURL) {
                                        let sdwebimage = SDWebImageSource(url: url, placeholder: UIImage(named: ""))
                                        self.arrProductImages.append(sdwebimage)
                                    }
                                }
                            }
                            self.imageSliderData()
                            self.CVMoreImages.reloadData()
                            if let typeVarients = data["variant"] as? [[String:Any]]{
                                self.arrVarients = ProductVarient.init(typeVarients).varients
                                self.varientTableView.reloadData()
                                self.height_tableView.constant = CGFloat(self.arrVarients.count * 100)
                            }
                            if self.arrVarients.count != 0 {
                                self.SelectVarientArray.removeAll()
                                self.height_tableView.constant = CGFloat(self.arrVarients.count * 100)
                                for varient in self.arrVarients {
                                    if varient.value?.count != 0 {
                                        self.SelectVarientArray.append(varient.value![0])
                                    }
                                }
                                let param: [String:Any] = ["product_id": self.productId,
                                                           "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                           "theme_id": APP_THEME_ID
                                ]
                                self.MakeAPICallforVarientStock(param)
                            }
                            let relatedParam: [String:Any] = ["product_id": self.productId,
                                                              "theme_id": APP_THEME_ID]
                            self.MakeAPICallForRelatedProducts(relatedParam)
//                            self.CVProduct.reloadData()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func notifyUser(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_notifyUser, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                               let massage = msg["message"] as! String
                               showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                        print(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeAPICallForRelatedProducts(_ param: [String:Any]) {
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_relatedProducts, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["data"] as? [[String:Any]]{
                            self.arrrelatedProducts.append(contentsOf: ProductsSuperModel.init(aData).product)
                            self.CVRelatedProducts.reloadData()
                            self.heightRelatedPro.constant = 350.0
                            self.lbl_related.isHidden = false
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforRelatedProGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_relatedProductsGuest, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["data"] as? [[String:Any]]{
                            self.arrrelatedProducts.append(contentsOf: ProductsSuperModel.init(aData).product)
                            self.CVRelatedProducts.reloadData()
                            self.heightRelatedPro.constant = 350.0
                            self.lbl_related.isHidden = false
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }

    func MakeAPICallforProductDetailGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_ProductDetailsGuest, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["product_info"] as? [String:Any]{
                                self.gestUserdata = aData
                                let proTitle = aData["name"] as? String
                                self.lbl_productname.text = proTitle
                                let proDescription = aData["description"] as? String
                                self.lbl_Discription.text = proDescription
                                let proPrice = aData["final_price"] as? String
                                self.lbl_price.text = proPrice
                                let categories = aData["category_name"] as? String
                                self.lbl_productCategories.text = "Category: \(categories!)"
                                self.lbl_categories.text = categories
                                let image = aData["cover_image_path"] as? String
                                let imageURL = getImageFullURL("\(image!)")
                                self.img_product.sd_setImage(with: URL(string: imageURL)) { image, error, type, url in
                                  self.img_product.image = image
                               }
                            if let discription = aData["other_description_array"] as? [[String:Any]]{
                                self.disciptions = Discription.init(discription).discription
                                self.descriptionTableView.reloadData()
                                self.height_discrition.constant = CGFloat(self.disciptions.count * 171)
                            }
                            let stock = aData["product_stock"] as? Int
                            self.stock = stock
                            let defaultvarientid = aData["default_variant_id"] as? Int
                            self.varienrid = defaultvarientid
                            if defaultvarientid == 0 {
                                if stock! <= 0 {
                                    self.lbl_OutofStock.isHidden = false
                                    self.btn_addtocart.setTitle("Notify me when available", for: .normal)
                                }else{
                                    self.lbl_OutofStock.isHidden = true
                                    self.btn_addtocart.setTitle("Add to cart", for: .normal)
                                }
                            }else{
                                if self.arrVarients.count != 0 {
                                    self.SelectVarientArray.removeAll()
                                    self.height_tableView.constant = CGFloat(self.arrVarients.count * 100)
                                    for varient in self.arrVarients {
                                        if varient.value?.count != 0 {
                                            self.SelectVarientArray.append(varient.value![0])
                                        }
                                    }
                                    let relatedParam: [String:Any] = ["product_id": self.productId,
                                                                      "theme_id": APP_THEME_ID]
                                    self.MakeAPICallforRelatedProGuest(relatedParam)
                                }
                            }
                            let review = aData["is_review"] as? Int
                            if review == 1 {
                                self.addReview.isHidden = true
                            }else if review == 0{
                                self.addReview.isHidden = true
                            }else{
                                self.addReview.isHidden = false
                            }
                        }
                        if let productReview = data["product_Review"] as? [[String:Any]]{
                            self.arrReviews = ProductReview.init(productReview).productreview
                            self.reviewCollection.reloadData()
                        }
                        if self.arrReviews.count != 0 {
//                            self.reteView.isHidden = false
                            self.height_rateing.constant = 270.0
                        }else{
//                            self.reteView.isHidden = true
                            self.height_rateing.constant = 0.0
                        }
                        self.arrProductImages.removeAll()
                        if let varient = data["product_image"] as? [[String:Any]]{
                            self.arrSubProduct = ProductSubImage.init(varient).subproductdata
                            let arrImage = self.arrSubProduct.map({$0.imagePath})
                            for producturl in 0..<arrImage.count{
                                let urlendpoint = arrImage[producturl]
                                if let endpoint = getImageFullURL(urlendpoint!) as? String {
                                    if let encodedURL = endpoint.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                                       let url = URL(string: encodedURL) {
                                        let sdwebimage = SDWebImageSource(url: url, placeholder: UIImage(named: ""))
                                        self.arrProductImages.append(sdwebimage)
                                    }
                                }
                            }
                            self.imageSliderData()
                            self.CVMoreImages.reloadData()
                            if let typeVarients = data["variant"] as? [[String:Any]]{
                                self.arrVarients = ProductVarient.init(typeVarients).varients
                                self.varientTableView.reloadData()
                                self.height_tableView.constant = CGFloat(self.arrVarients.count * 100)
                            }
                            if self.arrVarients.count != 0 {
                                self.SelectVarientArray.removeAll()
                                self.height_tableView.constant = CGFloat(self.arrVarients.count * 100)
                                for varient in self.arrVarients {
                                    if varient.value?.count != 0 {
                                        self.SelectVarientArray.append(varient.value![0])
                                    }
                                }
                                let param: [String:Any] = ["product_id": self.productId,
                                                           "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                           "theme_id": APP_THEME_ID
                                ]
                                self.MakeAPICallforVarientStock(param)
                            }
//                            self.CVProduct.reloadData()
                        }
                        let relatedParam: [String:Any] = ["product_id": self.productId,
                                                          "theme_id": APP_THEME_ID]
                        self.MakeAPICallforRelatedProGuest(relatedParam)
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforVarientStock(_ param:[String:Any]){
//        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_VarientStock, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }
                        let id = data["id"] as! Int
                        self.varientDetailsId = id
                        let varient = data["variant"] as! String
                        self.Selected_Variant_Name = varient
                        let finalPrice = data["final_price"] as! String
                        self.lbl_price.text = finalPrice
                        self.gestUserdata["final_price"] = finalPrice
                        let stock = data["stock"] as! Int
                        self.stock = stock
                        if stock <= 0 {
                            self.lbl_OutofStock.isHidden = false
                            self.btn_addtocart.setTitle("Notify me when available", for: .normal)
                        }else{
                            self.lbl_OutofStock.isHidden = true
                            self.btn_addtocart.setTitle("Add to cart", for: .normal)
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeApiCallForAddtoCart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
//            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productId,
                                                               "variant_id": self.varienrid!,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEME_ID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func MakeApiCallForAddtoCartDetails(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
//            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productDetalsId!,
                                                               "variant_id": self.varientDetailsId!,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEME_ID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func MakeAPIcallForAddreview(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
//            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductRateing, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                   let massage = stats["message"] as? String
                                   print(massage!)
                               }
                               print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        let param: [String: Any] = ["product_id":self.productId,
                                                                    "theme_id": APP_THEME_ID]
                                        self.MakeAPICallForRelatedProducts(param)
                                        self.CVRelatedProducts.reloadData()
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    func addrateing(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
//            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductRateing, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }

}
extension JewelleryDescriptionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CVMoreImages{
            return arrSubProduct.count
        }else if collectionView == CVRelatedProducts{
            return arrrelatedProducts.count
        }else if collectionView == reviewCollection{
            return arrReviews.count
        }
        else{
            return self.valueArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CVMoreImages{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
            cell.configureCell(arrSubProduct[indexPath.item])
            return cell
        }
        else if collectionView == CVRelatedProducts{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
            cell.configureCell(arrrelatedProducts[indexPath.row])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrrelatedProducts[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let imagepath = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": imagepath,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productId = self.arrrelatedProducts[indexPath.row].id!
                    self.varienrid = self.arrrelatedProducts[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrrelatedProducts[indexPath.row].id!,
                                               "variant_id": self.arrrelatedProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEME_ID]
                    self.MakeApiCallForAddtoCart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.btnFavUnfav.isHidden = true
            }else{
                cell.btnFavUnfav.isHidden = false
            }
            if arrrelatedProducts[indexPath.row].isinwishlist == false{
                cell.btnFavUnfav.setImage(UIImage(named: "ic_heart_empty"), for: .normal)
            }else{
                cell.btnFavUnfav.setImage(UIImage(named: "ic_heart_fill"), for: .normal)
            }
            cell.btnFavUnfav.tag = indexPath.row
            cell.btnFavUnfav.addTarget(self, action: #selector(onclickBtnWishlistFev), for: .touchUpInside)
            return cell
        }
        else if collectionView == reviewCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReViewCell", for: indexPath) as! ReViewCell
            cell.lbl_reviewText.text = arrReviews[indexPath.item].title!
            cell.lbl_reviewDiscription.text = arrReviews[indexPath.item].subtitle!
            cell.reviewView.rating = arrReviews[indexPath.item].reteing!
            cell.lbl_review.text = "\(arrReviews[indexPath.item].reteing!)"
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViweCell", for: indexPath) as! CollectionViweCell
            let data = self.valueArray[indexPath.item]
            cell.lbl_sizes.text = "\(data)"
            
            if data == self.SelectVarientArray[collectionView.tag]
            {
                cell.sizeView.backgroundColor = hexStringToUIColor(hex: "#0D1E1C")
                cell.sizeView.applyCornerRadius(8)
                cell.lbl_sizes.textColor = UIColor.white
            }
            else{
                cell.sizeView.backgroundColor = .clear
                cell.sizeView.applyBorder(.black, width: 1)
                cell.sizeView.applyCornerRadius(8)
                cell.lbl_sizes.textColor = UIColor.black
            }
            return cell
        }
      
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == CVMoreImages{
            return CGSize(width: 60, height: 60)
        }else if collectionView == CVRelatedProducts{
            return CGSize(width: collectionView.frame.width / 2, height: 350)
        }else if collectionView == reviewCollection{
            return CGSize(width: reviewCollection.frame.width, height: 244)
        }else{
            return CGSize(width: 90, height: 45)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CVMoreImages{
            self.bgimage.setCurrentPage(indexPath.row, animated: true)
        }else if collectionView == CVRelatedProducts{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "JewelleryDescriptionVC") as! JewelleryDescriptionVC
            vc.productId = arrrelatedProducts[indexPath.row].id!
            navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == reviewCollection {
            
        }else{
            let data = arrVarients[collectionView.tag].value!
            self.SelectVarientArray.remove(at: collectionView.tag)
            self.SelectVarientArray.insert(data[indexPath.item], at: collectionView.tag)
            self.varientTableView.reloadData()
            let param: [String:Any] = ["product_id": self.productId,
                                       "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                       "theme_id": APP_THEME_ID
            ]
            self.MakeAPICallforVarientStock(param)
        }
    }
    @objc func onclickBtnWishlistFev(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrrelatedProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
}
extension JewelleryDescriptionVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == varientTableView{
            return arrVarients.count
        }else{
            return disciptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == varientTableView{
            let varientdata = arrVarients[indexPath.row]
            let varientType = varientdata.type
            if varientType == "dropdown"{
                let cell = varientTableView.dequeueReusableCell(withIdentifier: "DropDownCell") as! DropDownCell
                cell.lbl_selectColorTitle.text = arrVarients[indexPath.row].name!
                cell.lbl_selectColor.text = arrVarients[indexPath.row].name
                cell.lbl_selectColor.text = self.SelectVarientArray[indexPath.row]
                self.varientColor = arrVarients[indexPath.row].value!
                cell.onclickButtonClosure = {
                    cell.txt_selectColor.textColor = .clear
                    cell.txt_selectColor.checkMarkEnabled = false
                    cell.txt_selectColor.optionArray = self.arrVarients[indexPath.row].value!
                    cell.txt_selectColor.resignFirstResponder()
                    cell.txt_selectColor.selectedRowColor = hexStringToUIColor(hex: "#173334")
                    cell.txt_selectColor.rowBackgroundColor  = hexStringToUIColor(hex: "#173334")
                    cell.txt_selectColor.didSelect(completion: { selected, index, id in
                        cell.lbl_selectColor.text = selected
    //                    cell.lbl_selectColor.text = self.SelectVarientArray[indexPath.row]
                        self.SelectVarientArray.remove(at: cell.txt_selectColor.tag)
                        self.SelectVarientArray.insert(selected, at: cell.txt_selectColor.tag)
                        self.varientTableView.reloadData()
                        let param: [String:Any] = ["product_id": self.productId,
                                                   "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                   "theme_id": APP_THEME_ID
                        ]
                        self.MakeAPICallforVarientStock(param)
                    })
                }
                return cell
            }else{
                let cell = varientTableView.dequeueReusableCell(withIdentifier: "SizeCell") as! SizeCell
                cell.lbl_selectSize.text = arrVarients[indexPath.row].name
                self.valueArray = arrVarients[indexPath.row].value!
                cell.collectionView.tag = indexPath.row
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.reloadData()
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as! DescriptionCell
            cell.lbl_description.text = disciptions[indexPath.row].title
            cell.lbl_descript.text = disciptions[indexPath.row].descrips
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == varientTableView{
            return 120
        }else{
            return 171
        }
    }
    
}
extension JewelleryDescriptionVC : FeedbackDelegate
{
    func refreshData(id: Int, rating_no: String, title: String, description: String) {
        let param: [String:Any] = ["id": id,
                                   "user_id": getID(),
                                   "rating_no": rating_no,
                                   "title": title,
                                   "description": description,
                                   "theme_id": APP_THEME_ID]
        self.addrateing(param)
    }
    
}
