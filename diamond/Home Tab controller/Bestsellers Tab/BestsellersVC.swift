//
//  BestsellersVC.swift
//  kiddos
//
//  Created by mac on 13/04/22.
//

import UIKit
import SDWebImage
import Alamofire
import ImageSlideshow

struct Images{
    var image = String()
}

class BestsellersVC: UIViewController, ImageSlideshowDelegate {
    
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        lbl_producname.text = arrFeatured[page].name
        lbl_proprice.text = arrFeatured[page].finalPrice
        lbl_category.text = arrFeatured[page].categoryName
        lbl_procurrency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)
    }
    

    @IBOutlet weak var lbl_cartCount: UILabel!
    @IBOutlet weak var CVProducts: UICollectionView!
    @IBOutlet weak var productsImage: ImageSlideshow!
    @IBOutlet weak var heightproductCV: NSLayoutConstraint!
    @IBOutlet weak var CVImages: UICollectionView!
    @IBOutlet weak var CVProducts1: UICollectionView!
    @IBOutlet weak var heightproduct1CV: NSLayoutConstraint!
    @IBOutlet weak var lblhome1title: UILabel!
    @IBOutlet weak var lblhome1Discription: UILabel!
    @IBOutlet weak var lblhome2title: UILabel!
    @IBOutlet weak var lblhome2Discription: UILabel!
    @IBOutlet weak var lblHomeHeader: UILabel!
    @IBOutlet weak var btnHomeCheckMoreProducts: UIButton!
    @IBOutlet weak var lblPromo1Title: UILabel!
    @IBOutlet weak var lblPromo1Subtitle: UILabel!
    @IBOutlet weak var lblPromo2Title: UILabel!
    @IBOutlet weak var lblPromo2Subtitle: UILabel!
    @IBOutlet weak var img1Icon: UIImageView!
    @IBOutlet weak var img2Icon: UIImageView!
    @IBOutlet weak var lblSubscriptionHeader: UILabel!
    @IBOutlet weak var heightCategoryTV: NSLayoutConstraint!
    @IBOutlet weak var lblSubscriptionDetail: UILabel!
    @IBOutlet weak var lblSubscriptionSub: UILabel!
    @IBOutlet weak var TVCategories: UITableView!
    @IBOutlet weak var lbl_category: UILabel!
    @IBOutlet weak var lbl_producname: UILabel!
    @IBOutlet weak var lbl_procurrency: UILabel!
    @IBOutlet weak var lbl_proprice: UILabel!
    
    var arrCategories : [categoryModel] = []
    var arrProducts:[Products] = []
    var arrFeatured:[Products] = []
    var arrProduts1: [Products] = []
    var firstCatID = Int()
    var arrFeaturedProducts: [FeaturedProduct] = []
    var arrProductImages: [SDWebImageSource] = []
    var arrStaticImages = [String]()
    var pageIndex = 1
    var lastIndex = 0
    var trendingPageindex = 1
    var trendinglastindex = 0
    var tmpIndex : Any?
    var addwishlist = String()
    var removeWishlist = String()
    var productid = Int()
    var varientId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitialUI()
        TVCategories.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellReuseIdentifier: "CategoriesCell")
        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        let baseParam: [String:Any] = ["theme_id": APP_THEME_ID]
        MakeAPICallForBaseURL(baseParam)
    }
    
    //MARK: - Custom Functions
    
    func setupInitialUI(){
        CVProducts.register(UINib.init(nibName: "ProductsCell", bundle: nil), forCellWithReuseIdentifier: "ProductsCell")
        CVImages.register(UINib.init(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCell")
        CVProducts1.register(UINib.init(nibName: "ProductsCell", bundle: nil), forCellWithReuseIdentifier: "ProductsCell")
    }
    
    func imageSliderData() {
        self.productsImage.slideshowInterval = 3.0
        self.productsImage.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.productsImage.contentScaleMode = UIView.ContentMode.scaleAspectFit
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.productsImage.pageIndicator = pageControl
        self.productsImage.setImageInputs(self.arrProductImages)
        self.productsImage.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.productsImage.addGestureRecognizer(recognizer)
    }
    @objc func didTapImage() {
        self.productsImage.presentFullScreenController(from: self)
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.parentVC = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickCart(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onclickBtnSearch(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SpecialGiftsVC") as! SpecialGiftsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - API
    
    func MakeAPICallForBaseURL(_ param: [String:Any]) {
        AIServiceManager.sharedManager.callPostApi(URL_BASE, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let baseURL = data["base_url"] as? String{
                            URL_BASE = baseURL
                        }
                        if let imageURL = data["image_url"] as? String{
                            URL_BASEIMAGE = imageURL
                        }
                        if let paymenturl = data["payment_url"] as? String{
                            URL_PaymentImage = paymenturl
                        }
                        if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                            let guestArray = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                            UserDefaults.standard.set(guestArray.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                        }
                        self.MakeAPICallforLandingPage(["theme_id":APP_THEME_ID])
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforLandingPage(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_LandingPage, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }
                        if let themeJson = data["them_json"] as? [String:Any]{
                            if let homeJson = themeJson["homepage-products"] as? [String:Any]{
                                self.lblHomeHeader.text = homeJson["homepage-products-label"] as? String
                                self.btnHomeCheckMoreProducts.setTitle(homeJson["homepage-products-btn"] as! String?, for: .normal)
                            }
                            if let homePromo1 = themeJson["homepage-promotions-1"] as? [String:Any]{
                                self.lblPromo1Title.text = homePromo1["homepage-promotions-title"] as? String
                                self.lblPromo1Subtitle.text = homePromo1["homepage-promotions-sub-text"] as? String
                                let iconEndpoint = homePromo1["homepage-promotions-promotion-icon"] as? String
                                let strUrl = getImageFullURL("\(iconEndpoint!)")
                                self.img1Icon.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "placeholder"))
                            }
                            if let homePromo2 = themeJson["homepage-promotions-2"]as? [String:Any]{
                                self.lblPromo2Title.text = homePromo2["homepage-promotions-title"] as? String
                                self.lblPromo2Subtitle.text = homePromo2["homepage-promotions-sub-text"] as? String
                                let iconEndpoint = homePromo2["homepage-promotions-promotion-icon"] as? String
                                let strUrl = getImageFullURL("\(iconEndpoint!)")
                                self.img2Icon.sd_setImage(with: URL(string: strUrl), placeholderImage: UIImage(named: "placeholder"))

                            }
                            if let homeBestProd1 = themeJson["homepage-best-product-1"] as? [String:Any]{
                                let producttitle = homeBestProd1["homepage-best-product-title"] as! String
                                self.lblhome1title.text = producttitle
                                let productDiscription = homeBestProd1["homepage-best-product-sub-text"] as! String
                                self.lblhome1Discription.text = productDiscription
                            }
                            if let homeBestProd2 = themeJson["homepage-best-product-2"] as? [String:Any]{
                                let title = homeBestProd2["homepage-best-product-title"] as? String
                                self.lblhome2title.text = title
                                let subtitle = homeBestProd2["homepage-best-product-sub-text"] as? String
                                self.lblhome2Discription.text = subtitle
                            }
                            if let homePRoduct = themeJson["homepage-products"] as? [String:Any] {
                                let imageArray = homePRoduct["homepage-products-bg-img"] as? [String]
                                self.arrStaticImages = imageArray!
                                self.CVImages.reloadData()
                            }
                            if let newsLetter = themeJson["homepage-newsletter"] as? [String:Any]{
                                self.lblSubscriptionHeader.text = newsLetter["homepage-newsletter-title"] as? String
                                self.lblSubscriptionDetail.text = newsLetter["homepage-newsletter-sub-text"] as? String
                                self.lblSubscriptionSub.text = newsLetter["homepage-newsletter-description"] as? String
                            }
                            self.MakeAPICallforCategoriesList(["theme_id":APP_THEME_ID])
                            self.MakeAPICalltogetCurrency(["theme_id":APP_THEME_ID])
                            self.pageIndex = 1
                            self.lastIndex = 0
                            self.trendingPageindex = 1
                            self.trendinglastindex = 0
                            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                                self.MakeAPICallforProductsGuest(["theme_id":APP_THEME_ID])
                                self.MakeAPICallforTrendingProductsGuest(["theme_id":APP_THEME_ID])
                                self.MakeAPICallforfeatureProductsGuest(["theme_id":APP_THEME_ID])
                            }else{
                                self.MakeAPICallforProducts(["theme_id":APP_THEME_ID])
                                self.MakeAPICallforTrending(["theme_id":APP_THEME_ID])
                                self.MakeAPICallforfeatureProducts(["theme_id":APP_THEME_ID])
                            }
                            
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func MakeAPICallforCategoriesList(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_HomeCategories, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let dataArr = data["data"] as? [[String:Any]]{
                                    self.arrCategories = HomeCatSuperModel.init(dataArr).categories
                                    self.heightCategoryTV.constant = CGFloat(228 * self.arrCategories.count)
                                    self.view.layoutSubviews()
                                    self.TVCategories.reloadData()
                                    let param: [String:Any] = ["theme_id": APP_THEME_ID]
                                    self.getextraURL(param)
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func MakeAPICallforProducts(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_Bestseller + "\(pageIndex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let aData = data["data"] as? [[String:Any]]{
                                    if self.pageIndex == 1 {
                                        let lastpage = data["last_page"] as! Int
                                        self.lastIndex = lastpage
                                        self.arrProducts.removeAll()
                                    }
                                    self.arrProducts.append(contentsOf: ProductsSuperModel.init(aData).product)
                                    self.CVProducts.reloadData()
                                    let count = json["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func MakeAPICallforTrending(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
          AIServiceManager.sharedManager.callPostApi(URL_TrendingProducts + "\(trendingPageindex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let aData = data["data"] as? [[String:Any]]{
                                    if self.trendingPageindex == 1 {
                                        let lastpage = data["last_page"] as! Int
                                        self.trendinglastindex = lastpage
                                        self.arrProduts1.removeAll()
                                    }
                                    self.arrProduts1.append(contentsOf: ProductsSuperModel.init(aData).product)
                                    self.CVProducts1.reloadData()
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforProductsGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_BestsellerGuest + "\(pageIndex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let aData = data["data"] as? [[String:Any]]{
                                   if self.pageIndex == 1 {
                                       let lastpage = data["last_page"] as! Int
                                       self.lastIndex = lastpage
                                       self.arrProducts.removeAll()
                                   }
                                   self.arrProducts.append(contentsOf: ProductsSuperModel.init(aData).product)
                                   self.CVProducts.reloadData()
                               }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func MakeAPICallforTrendingProductsGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_TrendingProductsGuest + "\(trendingPageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let aData = data["data"] as? [[String:Any]]{
                                    if self.trendingPageindex == 1 {
                                        let lastpage = data["last_page"] as! Int
                                        self.trendinglastindex = lastpage
                                        self.arrProduts1.removeAll()
                                    }
                                    self.arrProduts1.append(contentsOf: ProductsSuperModel.init(aData).product)
                                    self.CVProducts1.reloadData()
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforfeatureProducts(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_CategoryProduct, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let aData = data["data"] as? [[String:Any]]{
                                    self.arrFeatured = ProductsSuperModel.init(aData).product
                                    let arrFirst3 = self.arrFeatured[0...2]
                                    self.arrProductImages.removeAll()
                                    let arrImage = arrFirst3.map({$0.coverimagepath})
                                    for producturl in arrImage{
                                        let getproductURL = getImageFullURL("\(producturl!)")
                                        let sdwebimage = SDWebImageSource(url: URL(string: "\(getproductURL)")!, placeholder: UIImage(named: ""))
                                        self.arrProductImages.append(sdwebimage)
                                    }
                                    self.lbl_proprice.text = self.arrFeatured[0].finalPrice
                                    self.lbl_producname.text = self.arrFeatured[0].name
                                    self.lbl_category.text = self.arrFeatured[0].categoryName
                                    self.lbl_procurrency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)
                                    self.imageSliderData()
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func MakeAPICallforfeatureProductsGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_CategoryProductGuest, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1 {
                                if let aData = data["data"] as? [[String:Any]]{
                                    self.arrFeatured = ProductsSuperModel.init(aData).product
                                    let arrFirst3 = self.arrFeatured[0...2]
                                    self.arrProductImages.removeAll()
                                    let arrImage = arrFirst3.map({$0.coverimagepath})
                                    for producturl in arrImage{
                                        let getproductURL = getImageFullURL("\(producturl!)")
                                        let sdwebimage = SDWebImageSource(url: URL(string: "\(getproductURL)")!, placeholder: UIImage(named: ""))
                                        self.arrProductImages.append(sdwebimage)
                                    }
                                    self.lbl_proprice.text = self.arrFeatured[0].finalPrice
                                    self.lbl_producname.text = self.arrFeatured[0].name
                                    self.lbl_category.text = self.arrFeatured[0].categoryName
                                    self.lbl_procurrency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)
                                    self.imageSliderData()
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func MakeAPICalltogetCurrency(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Currency, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let currency = data["currency"] as? String
                                UserDefaults.standard.set(currency, forKey: userDefaultsKeys.KEY_CURRENCY)
                                let currencyName = data["currency_name"] as? String
                                UserDefaults.standard.set(currencyName, forKey: userDefaultsKeys.KEY_CURRENCYNAME)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    //                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productid,
                                                               "variant_id": self.varientId,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEME_ID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        let param: [String: Any] = ["theme_id": APP_THEME_ID]
                                        self.MakeAPICallforProducts(param)
                                        self.MakeAPICallforTrending(param)
                                        self.CVProducts.reloadData()
                                        self.CVProducts1.reloadData()
                                    }
                                    print(json)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    print(massage!)
                                }
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getextraURL(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Extra, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let terms = data["terms"] as? String
                                UserDefaults.standard.set(terms, forKey: userDefaultsKeys.KEY_TERMS)
                                let contectus = data["contact_us"] as? String
                                UserDefaults.standard.set(contectus, forKey: userDefaultsKeys.KEY_CONTECTUS)
                                let returnpolicy = data["return_policy"] as? String
                                UserDefaults.standard.set(returnpolicy, forKey: userDefaultsKeys.KEY_RETURNPOLICY)
                                let insta = data["insta"] as? String
                                UserDefaults.standard.set(insta, forKey: userDefaultsKeys.KEY_INSTA)
                                let youtube = data["youtube"] as? String
                                UserDefaults.standard.set(youtube, forKey: userDefaultsKeys.KEY_YOUTUVBE)
                                let massanger = data["messanger"] as? String
                                UserDefaults.standard.set(massanger, forKey: userDefaultsKeys.KEY_MASSANGER)
                                let twitter = data["twitter"] as? String
                                UserDefaults.standard.set(twitter, forKey: userDefaultsKeys.KEY_TWITTER)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
    }
}

extension BestsellersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
         return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CVProducts{
            return arrProducts.count
        }else if collectionView == CVProducts1{
            return arrProduts1.count
        }else{
            return arrStaticImages.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.CVImages{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
            cell.producsDetailsImage.sd_setImage(with: URL(string: getImageFullURL("\(arrStaticImages[indexPath.row])"))) { image, error, type, url in
                cell.producsDetailsImage.image = image
            }
            return cell
        }else if collectionView == CVProducts{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
            cell.configureCell(self.arrProducts[indexPath.item])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrProducts[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let imagepath = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": imagepath,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProducts[indexPath.row].id!
                    self.varientId = self.arrProducts[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProducts[indexPath.row].id!,
                                               "variant_id": self.arrProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEME_ID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.btnFavUnfav.isHidden = true
            }else{
                cell.btnFavUnfav.isHidden = false
            }
            if arrProducts[indexPath.row].isinwishlist == false{
                cell.btnFavUnfav.setImage(UIImage(named: "ic_heart_empty"), for: .normal)
            }else if arrProducts[indexPath.row].isinwishlist == true{
                cell.btnFavUnfav.setImage(UIImage(named: "ic_heart_fill"), for: .normal)
            }
            cell.btnFavUnfav.tag = indexPath.row
            cell.btnFavUnfav.addTarget(self, action: #selector(onclickFevBtn), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
            cell.configureCell(self.arrProduts1[indexPath.item])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrProduts1[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let imagepath = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": imagepath,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.displayName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProduts1[indexPath.row].id!
                    self.varientId = self.arrProduts1[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProduts1[indexPath.row].id!,
                                               "variant_id": self.arrProduts1[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEME_ID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.btnFavUnfav.isHidden = true
            }else{
                cell.btnFavUnfav.isHidden = false
            }
            if arrProduts1[indexPath.row].isinwishlist == false{
                cell.btnFavUnfav.setImage(UIImage(named: "ic_heart_empty"), for: .normal)
            }else{
                cell.btnFavUnfav.setImage(UIImage(named: "ic_heart_fill"), for: .normal)
            }
            cell.btnFavUnfav.tag = indexPath.row
            cell.btnFavUnfav.addTarget(self, action: #selector(onclickBtnWishlistFev), for: .touchUpInside)
            return cell
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.CVImages{
            return CGSize(width: 233, height: 268)
        }else if collectionView == self.CVProducts{
            return CGSize(width: collectionView.frame.width / 2, height: 350)
        }else{
            return CGSize(width: collectionView.frame.width / 2, height: 350)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CVProducts {
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "JewelleryDescriptionVC") as! JewelleryDescriptionVC
            vc.productId = arrProducts[indexPath.row].id!
            navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == CVProducts1{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "JewelleryDescriptionVC") as! JewelleryDescriptionVC
            vc.productId = arrProduts1[indexPath.row].id!
            navigationController?.pushViewController(vc, animated: true)
        }else {
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            if collectionView == CVProducts{
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.pageIndex != self.lastIndex{
                        self.pageIndex += 1
                        if arrProducts.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforProductsGuest(param)
                        }
                    }
                }
            }else if collectionView == CVProducts1{
                if indexPath.item == self.arrProduts1.count - 1 {
                    if self.trendingPageindex != self.trendinglastindex{
                        self.trendingPageindex += 1
                        if arrProduts1.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforTrendingProductsGuest(param)
                        }
                    }
                }
            }
        }else{
            if collectionView == CVProducts{
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.pageIndex != self.lastIndex{
                        self.pageIndex += 1
                        if arrProducts.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforProducts(param)
                        }
                    }
                }
            }else if collectionView == CVProducts1{
                if indexPath.item == self.arrProduts1.count - 1 {
                    if self.trendingPageindex != self.trendinglastindex{
                        self.trendingPageindex += 1
                        if arrProduts1.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEME_ID]
                            MakeAPICallforTrending(param)
                        }
                    }
                }
            }

        }
    }
    
    @objc func onclickFevBtn(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = true
                self.getWishList(param)
            }else if data.isinwishlist == true{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
    @objc func onclickBtnWishlistFev(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProduts1[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "add",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "remove",
                                           "theme_id": APP_THEME_ID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
    
}
extension BestsellersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell") as! CategoriesCell
        cell.selectionStyle = .none
        cell.configureCell(arrCategories[indexPath.row])
        cell.btnShowmore.tag = indexPath.row
        cell.btnShowmore.addTarget(self, action: #selector(onclickShowMoreProduct), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 228
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductListingVC") as! ProductListingVC
        vc.catid = arrCategories[indexPath.row].id!
        vc.isHome = "yes"
        vc.isMenu = "Menu"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onclickShowMoreProduct(sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ProductListingVC") as! ProductListingVC
        vc.catid = arrCategories[sender.tag].id!
        vc.isHome = "yes"
        vc.isMenu = "Menu"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
