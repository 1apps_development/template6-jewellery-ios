//
//  TrackOrderVC.swift
//  jewellery
//
//  Created by mac on 24/11/22.
//

import UIKit

class TrackOrderVC: UIViewController {
    
    @IBOutlet weak var img_circleOne: UIImageView!
    @IBOutlet weak var lbl_titleOne: UILabel!
    @IBOutlet weak var lbl_lineOne: UILabel!
    @IBOutlet weak var img_circleTwo: UIImageView!
    @IBOutlet weak var lbl_titleTwo: UILabel!
    @IBOutlet weak var lbl_linetwo: UILabel!
    
    var Status = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.Status == "Pending"
        {
            self.img_circleOne.image = UIImage(systemName: "circle.fill")
            self.img_circleOne.tintColor = hexStringToUIColor(hex: "#3E4F46")
            self.lbl_lineOne.backgroundColor = hexStringToUIColor(hex: "#3E4F46")
            
            self.img_circleTwo.image = UIImage(systemName: "circle")
            self.img_circleTwo.tintColor = .white
            self.lbl_linetwo.backgroundColor = .white
        }
        else if self.Status == "Delivered" {
            
            self.img_circleOne.image = UIImage(systemName: "circle.fill")
            self.img_circleOne.tintColor = hexStringToUIColor(hex: "#3E4F46")
            self.lbl_lineOne.backgroundColor = hexStringToUIColor(hex: "#3E4F46")
            
            self.img_circleTwo.image = UIImage(systemName: "circle.fill")
            self.img_circleTwo.tintColor = hexStringToUIColor(hex: "#3E4F46")
            self.lbl_linetwo.backgroundColor = hexStringToUIColor(hex: "#3E4F46")
        }else{
            print("cancel or return")
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onclickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
