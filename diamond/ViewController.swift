//
//  ViewController.swift
//  jewellery
//
//  Created by mac on 21/04/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["theme_id": APP_THEME_ID]
        getextraURL(param)
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickLogin(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickSignup(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickGuest(_ sender: Any){
        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_USERID)
        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_APP_TOKEN)
        let cartdata = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        if cartdata == "" {
            setValueToUserDefaults(value: "" as AnyObject, key: userDefaultsKeys.KEY_SAVED_CART)
        }
        
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: APIFunctios
    func getextraURL(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Extra, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let terms = data["terms"] as? String
                                UserDefaults.standard.set(terms, forKey: userDefaultsKeys.KEY_TERMS)
                                let contectus = data["contact_us"] as? String
                                UserDefaults.standard.set(contectus, forKey: userDefaultsKeys.KEY_CONTECTUS)
                                let returnpolicy = data["return_policy"] as? String
                                UserDefaults.standard.set(returnpolicy, forKey: userDefaultsKeys.KEY_RETURNPOLICY)
                                let insta = data["insta"] as? String
                                UserDefaults.standard.set(insta, forKey: userDefaultsKeys.KEY_INSTA)
                                let youtube = data["youtube"] as? String
                                UserDefaults.standard.set(youtube, forKey: userDefaultsKeys.KEY_YOUTUVBE)
                                let massanger = data["messanger"] as? String
                                UserDefaults.standard.set(massanger, forKey: userDefaultsKeys.KEY_MASSANGER)
                                let twitter = data["twitter"] as? String
                                UserDefaults.standard.set(twitter, forKey: userDefaultsKeys.KEY_TWITTER)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
    }

}

