//
//  TextDetailsCell.swift
//  jewellery
//
//  Created by mac on 21/11/22.
//

import UIKit

class TextDetailsCell: UICollectionViewCell {

    @IBOutlet weak var lbl_texttitle: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ textdetails: TextdataList){
        lbl_price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(textdetails.taxPrice!)"
        lbl_texttitle.text = textdetails.taxString
    }


}
