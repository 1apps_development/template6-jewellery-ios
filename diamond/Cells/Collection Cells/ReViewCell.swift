//
//  ReViewCell.swift
//  jewellery
//
//  Created by mac on 16/11/22.
//

import UIKit

class ReViewCell: UICollectionViewCell {

    @IBOutlet weak var lbl_client: UILabel!
    @IBOutlet weak var lbl_clientName: UILabel!
    @IBOutlet weak var lbl_reviewDiscription: UILabel!
    @IBOutlet weak var reviewView: CosmosView!
    @IBOutlet weak var lbl_reviewText: UILabel!
    @IBOutlet weak var lbl_review: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
