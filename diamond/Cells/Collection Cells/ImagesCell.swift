//
//  ImagesCell.swift
//  jewellery
//
//  Created by mac on 25/04/22.
//

import UIKit
import SDWebImage

class ImagesCell: UICollectionViewCell {

    @IBOutlet weak var producsDetailsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ subproducts: ProductImages){
        let subURL = getImageFullURL("\(subproducts.imagePath!)")
        producsDetailsImage.sd_setImage(with: URL(string: subURL)) { image, error, type, url in
            self.producsDetailsImage.image = image
        }
    }

}
