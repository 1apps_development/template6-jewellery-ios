//
//  ProductsCell.swift
//  kiddos
//
//  Created by mac on 13/04/22.
//

import UIKit
import SDWebImage

class ProductsCell: UICollectionViewCell {
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnAddtocart: UIButton!
    @IBOutlet weak var btnFavUnfav: UIButton!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var lblPrice: UILabel!

    var onclickAddCartClosure: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
   
    func configureCell(_ prod: Products){
        //        lblCategory.text = prod.
        lblCurrency.text = getCurrencyName()
        lblProductName.text = prod.name
        lblPrice.text = "\(prod.finalPrice!)"
        let endpoint = prod.coverimagepath
        let imgUrl = getImageFullURL("\(endpoint!)")
        imgProduct.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    @IBAction func onclickAddtocart(_ sender: UIButton){
        self.onclickAddCartClosure?()
    }
}
