//
//  CategoriesCell.swift
//  jewellery
//
//  Created by mac on 02/11/22.
//

import UIKit

class CategoriesCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCatname: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var btnShowmore: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ category:categoryModel){
//        lblTitle.text = category.title
        lblCatname.text = category.name
        let endPoint = category.imagePath
        let urlStr = getImageFullURL("\(endPoint!)")
        imgCategory.sd_setImage(with: URL(string: urlStr), placeholderImage: UIImage(named: "placeholder"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
