//
//  WishlistCell.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit

class WishlistCell: UITableViewCell {
    
    @IBOutlet weak var img_product: RoundableImageView!
    @IBOutlet weak var lbl_currencyName: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_varient: UILabel!
    @IBOutlet weak var lbl_productName: UILabel!
    
    var isWishlist:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(_ wishlists: WishlistData) {
        lbl_price.text = wishlists.finalprice
        let productimgURL = getImageFullURL("\(wishlists.productimage!)")
        img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            self.img_product.image = image
        }
        lbl_varient.text = wishlists.varientname
        lbl_productName.text = wishlists.productname
        lbl_currencyName.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
