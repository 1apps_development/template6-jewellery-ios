//
//  DelivaryCell.swift
//  jewellery
//
//  Created by mac on 23/11/22.
//

import UIKit

class DelivaryCell: UITableViewCell {

    @IBOutlet weak var img_selected: UIImageView!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var img_delivary: UIImageView!
    @IBOutlet weak var lbl_discription: UILabel!
    @IBOutlet weak var lbl_delivaryName: UILabel!
    @IBOutlet weak var cellView: ShadowView!
    @IBOutlet weak var aditionalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configCell(_ delivary: DelivaryList) {
        if delivary.chage_type == "percentage"{
            lbl_price.text = "\(delivary.amount!) %"
        }else{
            lbl_price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(delivary.amount!)"
        }
        let imageURL = getImageFullURL("\(delivary.imagepath!)")
        img_delivary.sd_setImage(with: URL(string: imageURL)) { image, error, type, url in
            self.img_delivary.image = image
        }
        lbl_discription.text = delivary.descript
        lbl_delivaryName.text = delivary.name
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
