//
//  AddressBookCell.swift
//  jewellery
//
//  Created by mac on 23/11/22.
//

import UIKit

class AddressBookCell: UITableViewCell {

    @IBOutlet weak var lbl_fullAddress: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
