//
//  CartCell.swift
//  kiddos
//
//  Created by mac on 18/04/22.
//

import UIKit

class CartCell: UITableViewCell {
    
    @IBOutlet weak var lbl_productName: UILabel!
    @IBOutlet weak var img_products: UIImageView!
    @IBOutlet weak var lbl_proprice: UILabel!
    @IBOutlet weak var btn_Plus: UIButton!
    @IBOutlet weak var lbl_finalprice: UILabel!
    @IBOutlet weak var lbl_provalue: UILabel!
    @IBOutlet weak var btn_proMinus: UIButton!
    @IBOutlet weak var lbl_proCurrency: UILabel!
    @IBOutlet weak var qty_proView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configCell(_ products: CartsList){
        lbl_productName.text = products.name
        let productimgURL = "\(products.image!)"
        img_products.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            self.img_products.image = image
        }
        let price = Double(products.finalprice!)! * Double(products.qty!)
        lbl_proprice.text = products.varientName!
        lbl_finalprice.text = "\(price)"
        lbl_provalue.text = "\(products.qty!)"
        lbl_proCurrency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
